<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'CertiFUNcation Example Extension',
    'description' => 'Just an example',
    'category' => 'plugin',
    'author' => 'Armin Vieweg',
    'author_email' => 'armin.vieweg@typo3.org',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0-dev',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
