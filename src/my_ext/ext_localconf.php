<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    // Do some stuff
};
$boot($_EXTKEY);
unset($boot);
